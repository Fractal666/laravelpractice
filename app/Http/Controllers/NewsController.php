<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Pasakome controlleriu kad naudosime news modeli
use App\NewsItem;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Gauname is newsItem Modelio visas naujienas ir issaugome i kintamaji
        $news = NewsItem::all();

        // Perduodame visas naujienas ($news) i savo view faila
        /* 1 parametras view failo pavadinimas */
        /* 2 parametras masyvas duomenu ka perduosime i view faila */
        /* 3 masyvo key kintamojo pavadinimas kaip i ji kreipsimes view faile */
        /* 4. masyvo value tai yra koki kintamaji perduodame is controlerio */
        return view("news" // resources/views/news.blade.php
            , ["news" // Kintamojo pavadinimas view faile
            => $news // Kintamojo pavadinimas controleryje
        ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $newsItem = new NewsItem();
        $newsItem->title = "Labas vakaras";
        $newsItem->content = "123";
        $newsItem->image = "http://www.mountsinai.on.ca/about_us/news/news-feature/img/demopage/image-3.jpg/image";
        $newsItem->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
